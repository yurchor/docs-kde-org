<?php
/*
 * Filename: 	functions.php
 * Description: supplies functions for the KDE Homepage.
 * Functions: 	printNews(items)
 * Author: 	Sebastian Faubel
 * Modified by: Jason Bainbridge and Chris Howells
 * Comments: Added some caching code for the rdf generation.
 * NB. Those functions should be merged together at sometime
 *     for the sake of cleaner code
 *	   + the html needs to be cleaned up
 */

function kde_menu ()
{
  global $site_title;
  global $site_root;
  global $menu_root;

  $m_root = getcwd();
  $menu_baseurl = dirname($_SERVER['PHP_SELF']);

  if (isset($menu_root))
  {
    if ($menu_root == "./")
      $s = "";
    else if (substr ($menu_root, -1) == "/")
      $s = $menu_root;
    else
      $s = $menu_root."/";
  }
  else
  {
    if (!isset ($site_root))
      $s = "/";
    else if ($site_root == "./")
      $s = "";
    else if (substr ($site_root, -1) == "/")
      $s = $site_root;
    else
      $s = $site_root."/";
  }

  if (substr ($s, 0, 1) == '/')
  {
    $c = substr_count ($menu_baseurl, "/");
    $i = 0;
    while ($i < $c)
    {
      $m_root = dirname ($m_root);
      $i++;
    }

    $menu_baseurl = "";
    $s = substr ($s, 1);
  }

  while (substr ($s, 0, 3) == "../")
  { $s = substr ($s, 3);
    $m_root = dirname ($m_root);
    $menu_baseurl = dirname ($menu_baseurl);
  }

  $m_root = substr ($m_root."/".$s, 0, -1);
  if ($menu_baseurl == "/")
    $menu_baseurl = substr ("/".$s, 0, -1);
  else $menu_baseurl = substr ($menu_baseurl."/".$s, 0, -1);

  // calculate the url of the current file relative to $menu_baseurl;
  $current_relativeurl = substr ($_SERVER['PHP_SELF'], strlen($menu_baseurl), strlen($_SERVER['PHP_SELF']) - strlen($menu_baseurl));
  if ($current_relativeurl[0] == "/")
    $current_relativeurl = substr ($current_relativeurl, 1, strlen($current_relativeurl));

  // return the created menu object
  return new BaseMenu($m_root, $menu_baseurl, $current_relativeurl);

  $site_root = $menu_baseurl;
}

function kde_general_news ($file, $items, $summaryonly)
{
  global $site_locale;
  startTranslation($site_locale);

  if ($summaryonly)
    print "<h2><a name=\"news\">" . i18n_var("Latest News") . "</a></h2>\n";

  $news = new RDF();
  $rdf_pieces = $news->openRDF($file);

  if(!$items)
  {
     $items = 5; // default
  }
  $rdf_items = count($rdf_pieces);
  if ($rdf_items > $items)
  {
     $rdf_items = $items;
  }

  //only open the file if it has something in it
  if ($rdf_items > 0)
  {
    /* Don't display the last story twice
    * if there is less than the requested number of stories
    * in the RDF file */
    if ($rdf_items < $items)
    {
      $rdf_items = $rdf_items - 1;
    }

   $alternate = false;

    print "<table style=\"width: 100%;\" class=\"table_box\" cellpadding=\"6\">\n";

    if ($summaryonly)
      print "<tr><th>" . i18n_var("Date") . "</th>\n<th>" . i18n_var("Headline") . "</th></tr>\n";

    $prevDate = "";
    for($x=1;$x<=$rdf_items;$x++)
    {
      $alternate = !$alternate;
      if ($alternate)
      {
        $color = "newsbox1";
      }
      else
      {
        $color = "newsbox2";
      }
      ereg("<title>(.*)</title>", $rdf_pieces[$x], $title);
      ereg("<date>(.*)</date>", $rdf_pieces[$x], $date);
      ereg("<fullstory>(.*)</fullstory>", $rdf_pieces[$x], $fullstory);
      print "<tr>\n";

      $printDate = $date[1];

      $title[1] = utf8_encode($title[1]);

      if ($summaryonly)
      {
        print "<td valign=\"top\" class=\"$color\">".(($printDate == $prevDate) ? "&nbsp;" : "<b>$printDate</b>")."</td>\n";
        print "<td class=\"$color\"><a href=\"news.php#item" . ereg_replace("[^a-zA-Z0-9]", "", $title[1]) . "\">$title[1]</a></td>\n";
        $prevDate = $printDate;
      }
      else
      {
        print "<td class=\"newsbox2\"><h3><a name=\"item" . ereg_replace("[^a-zA-Z0-9]", "", $title[1]) . "\">$printDate: $title[1]</a></h3></td>\n";
        print "</tr><tr><td class=\"newsbox2\">$fullstory[1]</td>\n";
      }

      print "</tr>\n";

    }
    print "</table>\n";
  }
}

function begin_under_construction()
{
  if ($_SERVER["QUERY_STRING"] != "test")
    print("<!-- Still Under Construction\n");
}

function end_under_construction()
{
  if ($_SERVER["QUERY_STRING"] != "test");
    print("     Still Under Construction -->\n");
}

function startTranslation($dictionary)
{
  global $site_root;
  global $site_external;
  $topleveldir = explode("/", $_SERVER['REQUEST_URI']);

  if ($site_root == "./") // if the page is not under a sub-directory
  {
    if (($site_external && ($_SERVER['SERVER_NAME'] != "kde.org")) || !$site_external) // if the page is something like kde.org or konqueror.org
    {
      $dir_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . "root.inc";
      $media_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
    else // something like kde.org/apps/konqueror or kde.org/areas/kde-ev
    {
      $dir_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . "root.inc";
      $media_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
  }
  else
  {
    if (($site_external && ($_SERVER['SERVER_NAME'] != "kde.org")) || !$site_external) // if the page is something like kde.org or konqueror.org
    {
      $dir_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . $topleveldir[1] . ".inc";
      $media_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
    else // someting like kde.org/apps/konqueror
    {
      $dir_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . $topleveldir[1] . ".inc";
      $media_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
  }

  if ($dictionary != "en")
  {
    global $text; //needed!
    if (file_exists($media_file))
    {
      include($media_file);
    }
    if (file_exists($dir_file))
    {
      include($dir_file);
    }
  }
}

function i18nDebug()
{
	global $text;
	print "text contains " . count($text) . " items \n";
}

function i18n_var($translate)
{
	global $text;
	global $site_locale;
	if ($site_locale == "en")
	{
		return $translate;
	}
	else
	{
		$translated = $text[$translate];
		if ($translated == "")
		{
			return $translate;
		}
		else
		{
			return $translated;
		}
	}
}

function i18n($translate)
{
	global $text;
	global $site_locale;
	if ($site_locale == "en")
	{
		print $translate;
	}
	else
	{
		$translated = $text[$translate];
		if ($translated == "")
		{
			print $translate;
		}
		else
		{
			print $translated;
		}
	}
}

function niceFileSize($path)
{
	if (file_exists($path))
	{
		$size = filesize($path);
		if ($size > (1024 * 1024))
		{
			echo "(" . round($size/(1024*1024)) . "MB)";
		}
		else
		if ($size > 1024)
		{
			echo "(" . round($size/1024) . "KB)";
		}
		else
		{
			echo "(" . round($size) . "B)";
		}
	}
	else
	{
		echo "(File not available on this server, use <a href=\"http://printing.kde.org\">printing.kde.org</a> or <a href=\"http://www.koffice.org\">koffice.org</a> instead)";
	}
}

function siteLogo($path, $width, $height)
{
	return "src=\"$path\" width=\"$width\" height=\"$height\"";
}

?>

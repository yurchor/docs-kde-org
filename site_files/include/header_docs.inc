<?php
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">

<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">

<head>
  <title>KDE Documentation - </title>
  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />

  <meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />

  <meta http-equiv=\"pics-label\" content='(pics-1.1 \"http://www.icra.org/ratingsv02.html\" comment \"ICRAonline DE v2.0\" l gen true for \"http://www.kde.org\"  r (nz 1 vz 1 lz 1 oz 1 cb 1) \"http://www.rsac.org/ratingsv01.html\" l gen true for \"http://www.kde.org\"  r (n 0 s 0 v 0 l 0))' />

  <meta name=\"trademark\" content=\"KDE e.V.\" />
  <meta name=\"description\" content=\"KDE.org\" />
  <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />
  <meta name=\"robots\" content=\"all\" />
  <meta name=\"no-email-collection\" content=\"http://www.unspam.com/noemailcollection\" />

  <link rel=\"shortcut icon\" href=\"/images/favicon.ico\" />


<link rel=\"stylesheet\" type=\"text/css\" href=\"//cdn.kde.org/css/jquery-ui.css\">
<link rel=\"stylesheet\" media=\"screen\" type=\"text/css\" title=\"Docs Default\" href=\"/styles/standard.css\" />
<link rel=\"stylesheet\" media=\"screen\" type=\"text/css\" title=\"Docs Default\" href=\"/docs.css\" />

  <link rel=\"alternative stylesheet\" media=\"screen, aural\" type=\"text/css\" title=\"High Contrast Dark\" href=\"/styles/darkbackground.css\" />
  <link rel=\"alternative stylesheet\" media=\"screen, aural\" type=\"text/css\" title=\"High Contrast Yellow on Blue\" href=\"/styles/yellowonblue.css\" />

<link rel=\"stylesheet\" media=\"print, embossed\" type=\"text/css\" href=\"/styles/print.css\" />
<script src=\"//cdn.kde.org/js/jquery.js\"></script>
<script src=\"//cdn.kde.org/js/jquery-ui.js\"></script>
<script>
$(function() {
  var availableApplications = $applications_list_json;
  $( \"#applicationlist\" ).autocomplete({
    autoFocus: true,
    source: availableApplications,
    appendTo: '#applicationform',
    select: function(event, ui) {
      $(this).val(ui.item.label);
      $(this).parents('form').submit();
    }
  });
});
</script>
<!-- Piwik -->
<script type=\"text/javascript\">
  var _paq = _paq || [];
  /* tracker methods like \"setCustomDimension\" should be called before \"trackPageView\" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u=\"//stats.kde.org/\";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '22']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->
</head>

<body id=\"docsnewkdeorg\" >

<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">

<!-- table row holding the header -->
<tr>
  <td>  
    <form action=\"/switch.php\" method=\"post\">
    <div id=\"nav_header_top\" align=\"right\">
      <a href=\"#content\" class=\"doNotDisplay\" accesskey=\"2\">Skip to main content ::</a>
    
      <a href=\"/\"><img id=\"nav_header_logo\" alt=\"Home\" align=\"left\" src=\"/images/kde_gear_64.png\" width=\"64\" height=\"64\" border=\"0\" /></a>
          <span class=\"doNotDisplay\">::</span>
      <img id=\"nav_header_logo_right\" alt=\"\" align=\"right\" src=\"/images/docs.png\" width=\"64\" height=\"64\" border=\"0\" />
        
      <div id=\"nav_header_title\" align=\"left\">KDE Documentation</div>
    
        
    </div>
    
    <div id=\"nav_header_bottom\" align=\"right\">
      <span id=\"nav_header_bottom_right\">
          <a href=\"http://www.kde.org/documentation/\" accesskey=\"6\" title=\"Having problems? Read the documentation\">Help</a> ::
          <a href=\"http://www.kde.org/contact/\" accesskey=\"10\" title=\"Contact information for all areas of KDE\">Contact Us</a>
      </span>
      <span class=\"doNotDisplay\">:: <a href=\"#navigation\" accesskey=\"5\">Skip to Link Menu</a><br/></span>
      <span id=\"nav_header_bottom_left\">
              &nbsp;</span>
    </div>
    </form>
  </td>
</tr>

<!-- table row holding the menu + main content -->
<tr>
  <td>
    <table id=\"main\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
    <tr>
          
      <td id=\"contentcolumn\" valign=\"top\"  >
        <div id=\"content\"><div style=\"width:100%;\">
        <a name=\"content\"></a>
";
?>

# Online KDE Documentation: docs.kde.org

## Maintainers

Current:
 - Luigi Toscano <luigi.toscano@tiscali.it>
 - Burkhard Lueck <lueck@hube-lueck.de>
 - Yuri Chornoivan <yurchor@ukr.net>

Past:
 - Rainer Endres <endres@kde.org>, 2005-12-13
 - Tom Albers <toma@kde.org>, 2009-03-14

## What is this?

A script to automatically fetch the newest version of the documentation from
Git (for English) and Subversion (for other languages) and build HTML and
PDF files from the DocBook sources.

A PHP framework to display the built documentation in an easy to use interface.

The Python script and the related modules replace the Bash script
(generator) which was in use from 2005 to 2015.
In turn, the previous script was based on the original Python
version by Daniel Naber.

### kdedocs.py

This script:
 - is configured through a ini-style configuration file;
 - fetches/updates the DocBook sources from Git (English) and
   Subversion (other languages) for the configured branches,
   languages and packages.
 - builds the HTML version from the sources using xsltproc
   and special XSLT stylesheets.
 - builds the PDF version from the sources using a custom version
   of dblatex;
 - builds a file with static data (generated_used.inc.php) which is used
   by the web application which shows the data to the user.

Requirements:
 - python 2.7
 - python-svn
 - python-git(>=0.3.6)
 - xsltproc
 - TeXLive (with various modules)

How does it work?

A periodic cronjob for user 'docs' should do everything automatically

It updates the scripts from https://invent.kde.org/websites/docs-kde-org.git

To use it manually, check the documentation from `./kdedocgen.py --help`.
A quite complete set of options is:
```
./kdedocgen.py -r -g -s -c docgen_conf.ini -l doclogconfig.ini
```

Main parameters:
```
 -c docgen_conf.ini specifies the configuration files with all
   the main parameters. The file part of the docs-kde-org repository
   is tuned for the docs.kde.org environment;
 -f forces a complete regeneration, ignoring the old files;
 -r retrieves the updated documentation from GIT and Subversion;
 -g generates the documentation and the static files needs by the PHP application;
 -s copies the static files to the website;
 --noenv don't regenerate the environments (kdelibs4, kf5, etc)
 -l specifies the python logging configuration file.
```
If you really want to find out what's wrong you can go into a folder and run things manually,
like:

```
xsltproc --catalogs xsltproc --stringparam kde.common "/$branchdir/common/" \
    $websiteDir/$branchdir/kdoctools/customization/kde-chunk-online.xsl index.docbook
/home/docs/docs/buildpdf.sh index.docbook
```

### logs

The included logging configuration files logs to `/home/docs/logs/`.

### search

Requirements:
- python-xapian
- xapian-omega

How does it work?

A cronjob for user `docs` should index everything automatically after the
generation completes.
All the stuff is located in the search/ folder, including the indexer
script, the Omega templates and configuration, and the wrapper script
which indexes everything and properly save the logs.

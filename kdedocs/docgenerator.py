#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2015  Luigi Toscano <luigi.toscano@tiscali.it>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from datetime import datetime
import glob
import json
import logging
import os
import shutil
from collections import Counter
from collections import namedtuple
from collections import OrderedDict

from .utils import compare_timestamp_file, find_files_pattern, mkdirp


LOGGER = logging.getLogger(__name__)


class ProgramDocData(object):
    """ Information about translated documentation for a specific program
    (for example, kdegraphics|okular or kde-runtime|kioslave/gzip).
    """
    __slots__ = ['_languages', '_namespaces']

    def __init__(self):
        self._languages = OrderedDict()
        self._namespaces = []

    @property
    def namespaces(self):
        return self._namespaces

    @property
    def languages(self):
        return self._languages.keys()

    def get_branches(self, lang, pkg=None):
        all_branches_pkgs = self._languages.get(lang, None)
        if not all_branches_pkgs:
            return []
        if not pkg:
            return [branchpkg[0] for branchpkg in all_branches_pkgs]
        else:
            return [branchpkg[0] for branchpkg in all_branches_pkgs
                    if branchpkg[1] == pkg]

    def add_namespace(self, namespace):
        if namespace not in self._namespaces:
            self._namespaces.append(namespace)

    def add_language(self, lang):
        if lang not in self._languages.keys():
            self._languages[lang] = []

    def add_branch(self, lang, branch, pkg):
        self.add_language(lang)
        self.add_namespace(pkg)
        if (branch, pkg) not in self._languages[lang]:
            self._languages[lang].append((branch, pkg))


class DocGenerator(object):

    JSON_PREFIX = 'SITEDATA'

    def __init__(self, config, env_manager):
        self.config = config
        self.env_manager = env_manager
        self.programs_doc_data = []

        excluded_lang_pdf_values = self.config.get_value('excluded_lang_pdf')
        self.excluded_lang_pdf = [lang.strip() for lang in
                                  excluded_lang_pdf_values.split(',')]
        LOGGER.debug('Excluding PDF generation for languages: %s' %
                     (self.excluded_lang_pdf))

    def _init_new_website(self):
        """Create the directory where the new website is built."""
        LOGGER.debug('Initializing new website directory')
        if os.path.isdir(self.config.nextwebsitedir):
            shutil.rmtree(self.config.nextwebsitedir)
        mkdirp(self.config.nextwebsitedir)

    def _init_branches(self, branches):
        generated_branches = branches
        if not generated_branches:
            generated_branches = self.config.enabled_branches
        for branch in generated_branches:
            LOGGER.info('Init generation for branch "%s"' % (branch))
            # FIXME: guard from invalid branch
            curr_env = self.env_manager.get_env(branch)
            curr_env.init_documentation_branch(branch)

    def _finalize_new_website(self):
        """Last steps of the generation of the new website: replace
        the old website with the new one."""
        LOGGER.debug('Finalizing the new website')
        currtimestamp = datetime.now().strftime("%Y%m%d%H%M")
        if os.path.isdir(self.config.websitedir):
            shutil.move(self.config.websitedir,
                        self.config.websitedir + currtimestamp)
        if os.path.isdir(self.config.nextwebsitedir):
            shutil.move(self.config.nextwebsitedir, self.config.websitedir)

    @staticmethod
    def are_docbooks_newer(base_dir, reference_file):
        """Returns true if at least one docbook in base_dir is newer
        than the referenced file."""

        try:
            # get each docbook and compare the timestamp
            for db_name in os.listdir(base_dir):
                if not db_name.endswith('.docbook'):
                    continue
                docbook_full_path = os.path.join(base_dir, db_name)
                docbook_time = os.path.getmtime(docbook_full_path)
                if compare_timestamp_file(docbook_time, reference_file) > 0:
                    return True
        except:
            return True
        return False

    @staticmethod
    def are_images_changed(old_img_dir, new_img_dir):
        """Returns true if images in new_img_dir are different from
        the images in old_img_dir (at least one changed/added/removed).
        """
        def get_pngs_set(base_img_dir):
            return set([img_name for img_name in os.listdir(base_img_dir)
                        if img_name.endswith('.png')])

        try:
            old_img_set = get_pngs_set(old_img_dir)
            new_img_set = get_pngs_set(new_img_dir)
            if old_img_set != new_img_set:
                # set changed, an image was added or removed
                return True
            for img in new_img_set:
                # size changed
                if (os.path.getsize(os.path.join(old_img_dir, img)) !=
                   os.path.getsize(os.path.join(new_img_dir, img))):
                    return True
            # TODO? at this point the content could be checked as well,
            # but it is really likely to be the same for images.
        except:
            return True
        return False

    def generate_documentation(self, forced=False, branches=None,
                               languages=None, packages=None):
        """ Generate the documentation """
        self._init_new_website()

        self._init_branches(branches)

        LOGGER.info('Starting generation (forced: %s)' % (forced))
        self.programs_doc_data = OrderedDict()
        for branch, lang, pkg, base_path in self.config.work_dir_walker(
                                            branches, languages, packages):
            # FIXME: guard from invalid branch
            curr_env = self.env_manager.get_env(branch)
            docbook_found = find_files_pattern(base_path, 'index.docbook')

            for ddir, _ in docbook_found:
                docbook_dir = os.path.join(base_path, ddir)
                currweb_dir = os.path.join(self.config.website_pkg_dir(
                    branch, lang, pkg), ddir)
                nextweb_dir = os.path.join(self.config.website_pkg_dir(
                    branch, lang, pkg, next=True), ddir)

                if lang != 'en':
                    original_en_docbook = os.path.join(self.config.op_pkg_dir(
                        branch, 'en', pkg), ddir, 'index.docbook')
                    if not os.path.isfile(original_en_docbook):
                        docbook_dir_rel = os.path.relpath(docbook_dir,
                                                          self.config.workdir)
                        LOGGER.debug('Skipping %s, no English version' %
                                     (docbook_dir_rel))
                        continue

                mkdirp(nextweb_dir)
                curr_env.copy_image_files(ddir, branch, lang, pkg)

                # FIXME: better handling of special case kcontrol/kio/etc
                program_name = ddir

                # check if the existing generated files (for HTML and PDF)
                # are really up-to-date with the source files. An additional
                # check is performed for PDF (the set of images)
                index_file = os.path.join(currweb_dir, 'index.html')
                if forced or self.are_docbooks_newer(docbook_dir, index_file):
                    curr_env.generate_doc_html(ddir, branch, lang, pkg)
                else:
                    # copy the existing HTMLs from the current site
                    html_files = [hf for hf in os.listdir(currweb_dir)
                                  if hf.endswith('.html')]
                    for hf in html_files:
                        shutil.copy2(os.path.join(currweb_dir, hf),
                                     os.path.join(nextweb_dir, hf))
                    LOGGER.debug('%s/%s/%s - html: up-to-date' % (lang, branch,
                                 program_name))

                if lang not in self.excluded_lang_pdf:
                    pdf_name = ddir.split('/')[-1] + '.pdf'
                    pdf_file = os.path.join(currweb_dir, pdf_name)
                    if (forced or self.are_docbooks_newer(docbook_dir,
                                                          pdf_file) or
                       self.are_images_changed(currweb_dir, nextweb_dir)):
                        curr_env.generate_doc_pdf(ddir, branch, lang, pkg)
                    else:
                        # copy the existing PDF from the current site
                        shutil.copy2(os.path.join(currweb_dir, pdf_name),
                                     os.path.join(nextweb_dir, pdf_name))
                        LOGGER.debug('%s/%s/%s - pdf: up-to-date' %
                                     (lang, branch, program_name))

                if program_name not in self.programs_doc_data:
                    self.programs_doc_data[program_name] = ProgramDocData()
                self.programs_doc_data[program_name].add_branch(lang, branch,
                                                                pkg)

        self._finalize_new_website()

    def write_generated_file(self, filename='generated_used.inc.php'):
        """ Write the files containing the list of available doc
        for each branch/language/program in json format (which will
        imported as PHP array and used accordingly). """
        LOGGER.info('Writing knowledge files (JSON)')
        modules_programs = {}
        programs_multiplemodules = {}
        programs_docs = {}
        for prog, progdocdata in self.programs_doc_data.items():
            # an application can have more than one namespace, even if
            # it is a rare occurence, but track it nevertheless.
            for namespace in progdocdata.namespaces:
                # prepare the list of programs for each module
                if namespace not in modules_programs:
                    modules_programs[namespace] = []
                modules_programs[namespace].append(prog)
            # save the exceptions (programs with branches in different
            # namespaces)
            if len(progdocdata.namespaces) > 1:
                LOGGER.debug('Found multiple namespaces (%s) for %s' %
                             (progdocdata.namespaces, prog))
                programs_multiplemodules[prog] = {}
                namespace_counter = {}
                try:
                    for namespace in progdocdata.namespaces:
                        for lang in self.config.configured_languages:
                            found_branches = progdocdata.get_branches(lang,
                                                                      namespace
                                                                      )
                            # count where a certain namespace is defined for
                            # each branch
                            for found_branch in found_branches:
                                if found_branch not in namespace_counter:
                                    namespace_counter[found_branch] = Counter()
                                namespace_counter[found_branch].update(
                                    [namespace]
                                )

                    # mark a certain branch of the program analysed as
                    # belonging to a specific namespace; consider the namespace
                    # most used amongst all languages (there should really be
                    # just one namespace regardless of the language, but there
                    # could be some leftovers in some languages...)
                    for abranch, ns_count in namespace_counter.items():
                        programs_multiplemodules[prog][abranch] = (
                            ns_count.most_common(1)[0][0]
                        )
                except Exception as e:
                    LOGGER.info('Exception on multinamespace for %s: %s' %
                                (prog, str(e)), exc_info=True)

            # main data: languages and branch for each program
            programs_docs[prog] = dict([(lang, progdocdata.get_branches(lang))
                                       for lang in progdocdata.languages])
        branch_description = {}
        for branch in self.config.enabled_branches:
            branch_desc = self.config.get_branch_value(branch, 'description')
            if not branch_desc:
                branch_desc = branch
            branch_description[branch] = branch_desc

        output_jsons = OrderedDict([
            ('programs_docs', programs_docs),
            ('modules_programs', modules_programs),
            ('programs_multiplemodules', programs_multiplemodules),
            ('languagelist', self.config.configured_languages),
            ('branch_description', branch_description)
        ])

        # remove old files
        json_pattern = os.path.join(self.config.workdir, '%s*.json' %
                                    (DocGenerator.JSON_PREFIX))
        for oldjsons in glob.glob(json_pattern):
            os.remove(oldjsons)

        # generate the new json files
        count = 0
        for data_name, data_content in output_jsons.items():
            filename = os.path.join(self.config.workdir, '%s%02d_%s.json' %
                                    (DocGenerator.JSON_PREFIX, count,
                                     data_name))
            count += 1
            with open(filename, 'w') as genfile:
                json.dump(data_content, genfile, indent=2)
            LOGGER.debug('Wrote knowledge file %s' % (filename))
